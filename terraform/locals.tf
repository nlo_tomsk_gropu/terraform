locals {
  env     = "develop"
  project = "platform"
  #role    = ["web", "db", "disk"]
  role    = ["controlnode", "worknode", "monitoring"]
  key_ssh = file("~/.ssh/id_rsa.pub")
}
